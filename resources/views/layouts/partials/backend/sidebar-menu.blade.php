<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">НАВИГАЦИЯ</li>
    <li class="{{ check_route(['dashboard::index', 'admin::index']) ? 'active': '' }}">
        <a href="{{ route('dashboard::index') }}">
            <i class="fa fa-dashboard"></i> <span>Панель</span>
        </a>
    </li>
    <li class="{{ check_route(['admin::users.index', 'admin::users.create']) ? 'active': '' }}">
        <a href="{{ route('admin::users.index') }}">
            <i class="fa fa-user-secret"></i> <span>Администраторы</span>
        </a>
    </li>
    <li class="{{ check_route(['admin::site-info.index']) ? 'active': '' }}">
        <a href="{{ route('admin::index') }}">
            <i class="fa fa-info"></i> <span>Информация Сайта</span>
        </a>
    </li>
</ul>
