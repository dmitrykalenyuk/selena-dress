<!DOCTYPE html>
<html lang="en">
<head>
    <title>Selena Dress</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Selena Dress shop project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{asset('/shop/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link href="{{asset('/shop/plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/styles/main_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/styles/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/styles/contact.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/shop/styles/contact_responsive.css')}}">
</head>
<body>

<div class="super_container">
    <!-- Header -->
    <header class="header">
        <div class="header_inner d-flex flex-row align-items-center justify-content-start">
            <div class="logo"><a href="#">Selena Dress</a></div>
            <nav class="main_nav">
                <ul>
                    <li><a href="{{route('index')}}">Главная</a></li>
                    <li><a href="categories.blade.php">Платья</a></li>
                    <li><a href="categories.blade.php">Аксесуары</a></li>
                    <li><a href="{{route('contact')}}">contact</a></li>
                </ul>
            </nav>
            <div class="header_content ml-auto">
                <div class="search header_search">
                    <form action="#">
                        <input type="search" class="search_input" required="required">
                        <button type="submit" id="search_button" class="search_button">
                            <img src="{{asset('shop/images/magnifying-glass.svg')}}" alt=""></button>
                    </form>
                </div>
                <div class="shopping">
                    <!-- Cart -->
                    <a href="#">
                        <div class="cart">
                            <img src="{{asset('shop/images/shopping-bag.svg')}}" alt="">
                            <div class="cart_num_container">
                                <div class="cart_num_inner">
                                    <div class="cart_num">1</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- Star -->
                    <a href="#">
                        <div class="star">
                            <img src="{{asset('shop/images/star.svg')}}" alt="">
                            <div class="star_num_container">
                                <div class="star_num_inner">
                                    <div class="star_num">0</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- Avatar -->
                    <a href="#">
                        <div class="avatar">
                            <img src="{{asset('shop/images/avatar.svg')}}" alt="">
                        </div>
                    </a>
                </div>
            </div>

            <div class="burger_container d-flex flex-column align-items-center justify-content-around menu_mm">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </header>

    <!-- Menu -->

    <div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
        <div class="menu_close_container">
            <div class="menu_close">
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="logo menu_mm"><a href="#">Selena Dress</a></div>
        <div class="search">
            <form action="#">
                <input type="search" class="search_input menu_mm" required="required">
                <button type="submit" id="search_button_menu" class="search_button menu_mm">
                    <img class="menu_mm" src="{{asset('shop/images/magnifying-glass.svg')}}" alt=""></button>
            </form>
        </div>
        <nav class="menu_nav">
            <ul class="menu_mm">
                <li class="menu_mm"><a href="#">Главная</a></li>
                <li class="menu_mm"><a href="#">Платья</a></li>
                <li class="menu_mm"><a href="#">Аксесуары</a></li>
                <li class="menu_mm"><a href="#">lingerie</a></li>
                <li class="menu_mm"><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>

@yield('content')


<!-- Footer -->

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="footer_logo"><a href="#">Selena Dress</a></div>
                    <nav class="footer_nav">
                        <ul>
                            <li><a href="{{route('index')}}">Главная</a></li>
                            <li><a href="categories.blade.php">Платья</a></li>
                            <li><a href="categories.blade.php">Аксессуары</a></li>
                            <li><a href="{{route('contact')}}">Контакты</a></li>
                        </ul>
                    </nav>
                    <div class="footer_social">
                        <ul>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="{{asset('shop/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('shop/styles/bootstrap4/popper.js')}}"></script>
<script src="{{asset('shop/styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{asset('shop/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('shop/plugins/easing/easing.js')}}"></script>
<script src="{{asset('shop/plugins/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{asset('shop/plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{asset('shop/js/custom.js')}}"></script>
</body>
</html>