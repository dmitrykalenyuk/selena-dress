<?php

namespace App;

/**
 * Class Reports
 *
 * A Helper Class For Counters in admin panel
 *
 * @package App
 */
class Reports
{
    /**
     * @return int
     */
    public static function getTotalUsers()
    {
        return User::query()->count();
    }
}
