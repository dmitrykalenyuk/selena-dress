<?php

namespace App\Http\Controllers\Shop;

/**
 * Class ViewController
 * Responsible for rendering for all views in project
 *
 * @package App\Http\Controllers\Shop
 */
class ViewController
{
    /**
     * Index Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('shop.pages.index');
    }

    /**
     * Contact Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('shop.pages.contact');
    }
}