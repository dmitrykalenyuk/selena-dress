<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Models\Shop
 *
 * @property integer  $id
 * @property string   $title       Product Title
 * @property  string  $description Product Description
 * @property float    $price       Product Price
 * @property integer  $category_id Id Of Related Category
 * @property Category $category    Object Of Related Category
 */
class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'title',
        'description',
        'price',
        'category_id',
    ];

    /**
     * Get Related Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * Get Images For Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class, 'product_id', 'id');
    }
}
