<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models\Shop
 *
 * @property string  $title    Category Title
 * @property integer parent_id Id Of Parent Category
 */
class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'title',
        'parent_id'
    ];
}
