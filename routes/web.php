<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$ctrl = 'Shop\ViewController';
Route::get('/', $ctrl . '@index')->name('index');
Route::get('/contact', $ctrl . '@contact')->name('contact');


Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    $ctrl = 'LoginController';
    Route::get('login', $ctrl . '@showLoginForm')->name('login');
    Route::post('login', $ctrl . '@login');
    Route::post('logout', $ctrl . '@logout')->name('logout');

    // Password Reset Routes...
    Route::group(['prefix' => 'password'], function () {
        $ctrl = 'ForgotPasswordController';
        Route::get('reset', $ctrl . '@showLinkRequestForm')->name('password.request');
        Route::post('email', $ctrl . '@sendResetLinkEmail')->name('password.email');
        Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('reset', 'ResetPasswordController@reset');
    });
});

Route::group(['middleware' => 'auth'], function () {
    /* Dashboard. Common access. */
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard::'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
        Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@showProfile']);
        Route::post('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@updateProfile']);
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin::', 'middleware' => ['admin']], function () {
            $ctrl = 'IndexController';
            Route::get('/', $ctrl . '@index')->name('index');
            Route::resource('users', 'UsersController');
            Route::resource('settings', 'SettingsController');
        });

});
